package com.example.hospitalreviewgitlab.controller;

import com.example.hospitalreviewgitlab.domain.dto.UserJoinRequest;
import com.example.hospitalreviewgitlab.execption.AppException;
import com.example.hospitalreviewgitlab.execption.ErrorCode;
import com.example.hospitalreviewgitlab.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserService userService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    @WithMockUser
    void 회원가입_성공() throws Exception {
        String userName = "s";
        String password = "123";

        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserJoinRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isOk());
    }
    @Test
    @WithMockUser
    void 회원가입_실패() throws Exception {
        String userName = "s";
        String password = "123";

        when(userService.join(any())).thenThrow(new AppException(ErrorCode.DUPLICATED_USERNAME, "오류"));

        mockMvc.perform(post("/api/v1/users/join")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserJoinRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isConflict());
    }
    @Test
    @WithMockUser
    void 로그인_성공() throws Exception {
        String userName = "s";
        String password = "123";

        when(userService.login(any())).thenReturn("token");

        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserJoinRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser
    void 로그인_실패_아이디없음() throws Exception {
        String userName = "s";
        String password = "123";

        when(userService.login(any())).thenThrow(new AppException(ErrorCode.NOT_FOUNT_USERNAME,"username 없음"));

        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserJoinRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
    @Test
    @WithMockUser
    void 로그인_실패_비밀번호오류() throws Exception {
        String userName = "s";
        String password = "123";

        when(userService.login(any())).thenThrow(new AppException(ErrorCode.UNAUTHORIZED_PASSWORD,"password 오류"));

        mockMvc.perform(post("/api/v1/users/login")
                        .with(csrf())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsBytes(new UserJoinRequest(userName, password))))
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }
}