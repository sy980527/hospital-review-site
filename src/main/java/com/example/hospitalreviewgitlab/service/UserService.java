package com.example.hospitalreviewgitlab.service;

import com.example.hospitalreviewgitlab.domain.User;
import com.example.hospitalreviewgitlab.domain.dto.UserJoinRequest;
import com.example.hospitalreviewgitlab.domain.dto.UserLoginRequest;
import com.example.hospitalreviewgitlab.execption.AppException;
import com.example.hospitalreviewgitlab.execption.ErrorCode;
import com.example.hospitalreviewgitlab.repository.UserRepository;
import com.example.hospitalreviewgitlab.util.JwtUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;

    @Value("${jwt.token.secret}")
    private String secretKey;
    private Long expiredTimeMs = 1000 * 60 * 60l;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder encoder) {
        this.userRepository = userRepository;
        this.encoder = encoder;
    }
    public String join(UserJoinRequest request) {
        //Id가 중복이면 에러처리
        userRepository.findByUserName(request.getUserName())
                .ifPresent(user -> {
                    throw new AppException(ErrorCode.DUPLICATED_USERNAME,request.getUserName() + " 해당 UserName이 중복됩니다.");
                });
        //회원가입 .save()
        //save를 하면 DB에서 자동으로 id값을 부여해준다.
        User saveUser = User.builder()
                .userName(request.getUserName())
                .password(encoder.encode(request.getPassword()))
                .build();
        userRepository.save(saveUser);

        return "회원가입이 완료되어습니다.";
    }

    public String login(UserLoginRequest userLoginRequest) {
        // 아이디 체크
        User user = userRepository.findByUserName(userLoginRequest.getUserName())
                .orElseThrow(() -> {
                    throw new AppException(ErrorCode.NOT_FOUNT_USERNAME, userLoginRequest.getUserName() + "이 없습니다.");
                });
        // 비밀번호 체크
        if (!encoder.matches(userLoginRequest.getPassword(), user.getPassword())) {
            throw new AppException(ErrorCode.UNAUTHORIZED_PASSWORD, "password가 일치하지 않습니다.");
        }
        //토큰 발행
        String token = JwtUtil.createToken(user.getUserName(), secretKey, expiredTimeMs);

        return token;
    }
}