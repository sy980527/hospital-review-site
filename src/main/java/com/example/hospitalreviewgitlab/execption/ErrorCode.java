package com.example.hospitalreviewgitlab.execption;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@AllArgsConstructor
public enum ErrorCode {
    DUPLICATED_USERNAME(HttpStatus.CONFLICT, ""),
    NOT_FOUNT_USERNAME(HttpStatus.NOT_FOUND,""),
    UNAUTHORIZED_PASSWORD(HttpStatus.UNAUTHORIZED,"")
    ;
    private HttpStatus httpStatus;
    private String message;
}
