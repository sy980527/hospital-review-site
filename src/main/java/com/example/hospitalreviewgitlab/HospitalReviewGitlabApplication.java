package com.example.hospitalreviewgitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HospitalReviewGitlabApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitalReviewGitlabApplication.class, args);
    }

}
